#include <type_traits>
#include <iostream>
#include <vector>
#include <set>


using namespace std;

// zad 3
// algorytm ma nastepujacy zalozenia (niestety niezbedne dla tablic w stylu C)
// tablice sa sobie rowne i posortowane rosnaco
// size to liczba elementow ( w sensie normalnym - nie liczonym od zera)
// zrobilem to jako szablon, bo w sumie dlaczego nie ;)


template <typename T>
bool isDisjointSet(int size, const T arr1[], const T arr2[])
{

    for (int i=size-1;i>=0;i--)
    {
//    cout << "comparing " << arr1[i] << "& " << arr2[i] << '\n';
    if (arr1[i]==arr2[i])
    {
        return false;
    }
    if(arr1[i]<arr2[i])
    {
    for (int j=i-1;arr1[i]<=arr2[j] && j>=0;j--)
    {
//   cout << "comparing " << arr1[i] << "& " << arr2[j] << '\n';

        if (arr1[i]==arr2[j])
        {
            return false;
        }

    }
    }
    else
    {
        for (int j=i+1;arr1[i]>=arr2[j] && j<size;j++)
        {
//      cout << "comparing " << arr1[i] << "& " << arr2[j] << '\n';
            if (arr1[i]==arr2[j])
            {
                return false;
            }

        }

    }
    }
    return true;
}

template <typename T>
bool isDisjointSet(const vector<T> & arr1,const vector<T> & arr2)
{

for(auto i=arr1.begin();i!=arr1.end();i++)
{
    if (arr1.size()==0 || arr2.size() == 0)
    {
        return true;
    }

    auto temp = std::find(arr2.begin(),arr2.end(),*i);
    if (temp!=arr2.end())
    {
        return false;
    }

}
return true;


}


uint Silnia(uint Var)
{
    if (Var==1 || Var==0)
    {
        return 1;
    }

    return Var * Silnia(Var-1);

}
int NWD(int a, int b)
{

    if (b==0)
    {
        return abs(a);
    }

    NWD(b, (a % b));
}


int main()
{
    cout << "Rozlacznosc zbiorow" << endl;
    int tab1[]={1,2,5,8,10,16,20,25};
    int tab2[]={0,3,6,9,11,14,17,26};
    bool result=  isDisjointSet<int>(8,tab1,tab2);
    cout << "Wynik (1-zbiory rozlaczne, 0-zbiory posiadaja wspolny element): " << result << '\n';

    cout << "Silnia" << '\n';
    cout << "Wynik:" << Silnia(8) << '\n';


    cout << "NWD" << '\n';
    cout << "Wynik: " << NWD(60,144) << '\n';
    return 0;

}
